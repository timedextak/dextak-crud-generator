# Laravel CRUD Generator

Este pacote Laravel Generator fornece e gera Controller, Model (com reloquent relations) e Views no Bootstrap para o desenvolvimento de suas aplicações com um único comando.

- Permite criar **Model** com Eloquent relations
- Permite criar **Controller** com todos os recursos
- Permite criar **views** em Bootstrap

## Requisitos
    Laravel >= 5.5
    PHP >= 7.1

## Instalação
1 - Instalar via composer
```
composer require dextak/crud-generator:dev-master
```
2- Ative como comando artisan
```
php artisan vendor:publish --tag=crud
```

## Como usar
```
php artisan make:crud {nome_da_tabela}

```

Adicione rota para o crud no `web.php`
```
Route::resource('{nome_da_tabela}', 'BankController');
```
Acrescente um "s" ao final do nome da tabela no momento da criação da rota

#### Opcionais
 - Rota custormizada
```
php artisan make:crud {nome_da_tabela} --route={nome_da_rota}
```

#### Incluir no layout do painel

```
<script src="{{ url('/') }}/js/interacaoGeral.js"></script>
<script src="{{ url('/') }}/assets/bundles/izitoast/js/iziToast.min.js"></script>
<script src="{{ url('/') }}/assets/js/page/toastr.js"></script>
```