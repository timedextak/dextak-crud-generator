/**
 Usar:
 <script src="{{ url('/') }}/assets/bundles/sweetalert/sweetalert.min.js"></script>
 * **/

function confirmarExlusaoGeral(formulario) {
    swal({
        title: 'Deseja realmente eliminar este registro ?',
        text: '',
        icon: 'warning',
        buttons: true,
        dangerMode: true,
        buttons: ["Não", "Sim"],
    })
        .then((willDelete) => {
            if (willDelete) {
                formulario.submit();
            } else {
                return false;
            }
        });

    return false;
}
